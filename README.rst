Zephyr Project Docker Image for ARM boards
==========================================

This docker image makes it easy to get going with building and running apps based on the Zephyr Project RTOS for ARM boards.

What's included
---------------

* Based on debian:sid
* Zephyr toolchain
