from debian:sid

RUN apt-get update && apt-get install -y --no-install-recommends eatmydata

RUN apt-get update && eatmydata apt-get install -y --no-install-recommends git cmake ninja-build gperf \
  ccache doxygen dfu-util device-tree-compiler \
  xz-utils file make wget \
  python3-ply python3-pip python3-setuptools python3-wheel python3-yaml python3-pyelftools

WORKDIR /root

RUN wget https://raw.githubusercontent.com/zephyrproject-rtos/zephyr/master/scripts/requirements.txt && \
    eatmydata pip3 install --user -r requirements.txt

RUN wget https://github.com/zephyrproject-rtos/meta-zephyr-sdk/releases/download/0.9.2/zephyr-sdk-0.9.2-setup.run && \
    eatmydata sh zephyr-sdk-0.9.2-setup.run

ENV ZEPHYR_GCC_VARIANT=zephyr
ENV ZEPHYR_SDK_INSTALL_DIR=/opt/zephyr-sdk
ENV CCACHE_DIR=/tmp/ccache
